from sabertooth import Sabertooth


class RoverCommands:
    def __init__(self, ramp_val=100):
        self.saber = Sabertooth("UART1", "ttyO1")
        self.set_ramp(ramp_val)

    def drive_motor(self, motor="both", direction="fwd", speed=100):
        self.saber.driveMotor(motor, direction, speed)

    def stop_motor(self):
        self.saber.stop()

    def set_ramp(self, ramp_val):
        self.saber.setRamp(ramp_val)