import threading
import const as C

from rover_commands import RoverCommands


class ButtonActions():
    def __init__(self):
        return

    def parse_button_action(self, button, action):
        button_action = C.buttons[button]

        if button_action[0] == "left":
            if action == "on":
                C.left_wheel_state = button_action[1]
            else:
                C.left_wheel_state = "off"
        elif button_action[0] == "right":
            if action == "on":
                C.right_wheel_state = button_action[1]
            else:
                C.right_wheel_state = "off"

    def button_action(self):
        motor_action = MotorActions()

        if C.left_wheel_state == 'forward':
            motor_action.left_forward()
        elif C.left_wheel_state == 'reverse':
            motor_action.left_reverse()

        if C.right_wheel_state == 'forward':
            motor_action.right_forward()
        elif C.right_wheel_state == 'reverse':
            motor_action.right_reverse()


class TriggerActions(object):
    def __init__(self):
        return


class ThumbStickActions(object):
    def __init__(self):
        self.ts = MotorActions()

    def thumbstick_action(self, thumbstick, direction, value):
        motor_action = MotorActions()
        t = None

        # TODO: use left-forward and other directions

        if thumbstick == 'left':
            if direction == 'left-forward':
                t = threading.Thread(target=motor_action.left_forward, args=('on', value))
            elif direction == 'left-reverse':
                t = threading.Thread(target=motor_action.left_reverse, args=('on', value))
            elif direction == 'right-forward':
                t = threading.Thread(target=motor_action.right_forward, args=('on', value))
            elif direction == 'right-reverse':
                t = threading.Thread(target=motor_action.right_reverse, args=('on', value))


        t.daemon = True
        t.start()


class MotorActions(object):
    def __init__(self):
        self.cmd = RoverCommands()

    def left_forward(self, action='on', y_value=100):
        print 'left-forward: %s' % action

        if action == "on":
            self.cmd.drive_motor("left", "fwd", y_value)
        else:
            self.cmd.drive_motor("left", "fwd", 0)

    def left_reverse(self, action='on', y_value=100):
        print 'left-reverse: %s' % action

        if action == "on":
            self.cmd.drive_motor("left", "rev", y_value)
        else:
            self.cmd.drive_motor("left", "rev", 0)

    def right_forward(self, action='on', x_value=100):
        print 'right-forward: %s' % action

        if action == "on":
            self.cmd.drive_motor("right", "fwd", x_value)
        else:
            self.cmd.drive_motor("right", "fwd", 0)

    def right_reverse(self, action='on', x_value=100):
        print 'right-reverse: %s' % action

        if action == "on":
            self.cmd.drive_motor("right", "rev", x_value)
        else:
            self.cmd.drive_motor("right", "rev", 0)
