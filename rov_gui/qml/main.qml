import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

ApplicationWindow {
    id: appWin
    title: settings.appName
    visible: true
    visibility: ApplicationWindow.FullScreen

    property color textColor: "#83F52C"
    property real textPixelSize: 0.025*height
    property real itemHeight: 2*textPixelSize
    property real buttonHeight: 0.08*height
    property real buttonMargin: 0.008*height

    function onLogin(userEmail, userPassword) {
        //TODO: check
        stackView.push(Qt.resolvedUrl("qrc:/qml/CameraView.qml"))
    }
    function onLogout() {
        if (1 < stackView.depth) {
            stackView.pop()
        }
    }

    style: ApplicationWindowStyle {
        background: Rectangle {
            anchors.fill: parent
            color: "black"
        }
    }

    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: Qt.resolvedUrl("qrc:/qml/LoginView.qml")
    }
}
