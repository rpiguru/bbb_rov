import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {
  id: custom_field_frame
  property alias sourceIcon: field_icon.source
  property alias placeholderText: field.placeholderText
  property alias echoMode: field.echoMode
  property alias text: field.text

  border.width: 1
  border.color: color

  color: "black"
  radius: 3
  TextField {
    id: field
    style: TextFieldStyle {
      textColor: appWin.textColor
      font.pixelSize: 0.25*control.height
      placeholderTextColor: textColor
      background: Rectangle {
        color: "transparent"
      }
    }
    anchors.left: parent.left
    anchors.right: field_icon.left
    anchors.top: parent.top
    anchors.topMargin: 1
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 1
    anchors.leftMargin: 0.02*parent.width
    anchors.rightMargin: field_icon.rightMargin
    verticalAlignment: TextInput.AlignVCenter
    onActiveFocusChanged: {
        if (activeFocus) {
            custom_field_frame.border.color = "lightblue"
        } else {
            custom_field_frame.border.color = custom_field_frame.color
        }
    }
  }
  Image {
    id: field_icon
    anchors.right: parent.right
    anchors.rightMargin: 0.05*parent.width
    anchors.verticalCenter: parent.verticalCenter
    width: 0.35*parent.height
    height: width
    smooth: true
    fillMode: Image.PreserveAspectFit
  }
}
