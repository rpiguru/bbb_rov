import QtQuick 2.7
import QtQuick.Controls 1.4

Item {
    id: loginView
    CustomTextFieldWithIcon {
        id: email
        x: 0.37*parent.width
        y: 0.407*parent.height
        width: parent.width-2*x
        height: 0.088*parent.height
        radius: 3
        sourceIcon: "qrc:/img/EmailIcon.svg"
        placeholderText: qsTr("Email...")
    }
    CustomTextFieldWithIcon {
        id: password
        anchors.top: email.bottom
        anchors.topMargin: 1
        anchors.left: email.left
        anchors.right: email.right
        height: email.height
        radius: email.radius
        sourceIcon: "qrc:/img/PasswordIcon.svg"
        placeholderText: qsTr("Password...")
        echoMode: TextInput.Password
    }
    CustomButtonStyle {
      id: btnStyle
    }
    Button {
        id: login
        anchors.top: password.bottom
        anchors.topMargin: appWin.buttonMargin
        anchors.right: parent.horizontalCenter
        anchors.rightMargin: appWin.buttonMargin
        width: password.width/2-2*anchors.rightMargin
        height: appWin.buttonHeight
        text: qsTr("Log In")
        isDefault: true
        style: btnStyle
        action: Action {
            shortcut: "Return"
            enabled: loginView.visible
        }
        onClicked: {
            appWin.onLogin(email.text, password.text)
        }
    }
    Button {
        id: cancel
        anchors.top: login.top
        anchors.left: parent.horizontalCenter
        anchors.leftMargin: appWin.buttonMargin
        width: login.width
        height: appWin.buttonHeight
        text: qsTr("Cancel")
        style: btnStyle
        action: Action {
            shortcut: "Esc"
            enabled: loginView.visible
        }
        onClicked: {
            Qt.quit()
        }
    }
}
