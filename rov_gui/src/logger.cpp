#include <QDir>
#include <QDate>
#include <QTimer>
#include <QThread>
#include <QDebug>
#include <QApplication>
#include "logger.h"

#define APPLICATION_FOLDER ".rov"
#define LOG_FILE_FOLDER "logs"
#define LOG_FILE_NAME "app.log"

Logger Logger::_instance;

Logger &Logger::instance()
{
    return _instance;
}

Logger::Logger() : _appFolder(QDir::homePath()+QDir::separator()+QString(APPLICATION_FOLDER)), _logFileName(_appFolder+QDir::separator()+QString(LOG_FILE_FOLDER)+QDir::separator()+QString(LOG_FILE_NAME))
{
    //create application and logs folders
    createFolder(QDir::homePath(), QString(APPLICATION_FOLDER));
    createFolder(QDir::homePath()+QDir::separator()+QString(APPLICATION_FOLDER), QString(LOG_FILE_FOLDER));

    //rename previous log file, if last modification time is not today, then open the log file
    renameLogFile(false);

    //setup message handler
    _textStream.setDevice(&_logFile);
    qInstallMessageHandler(debugMessageHandler);
}

void Logger::createFolder(const QString &path, const QString &folderName)
{
    //check if folder exists
    QDir logDir(path+QDir::separator()+folderName);
    if (!logDir.exists()) {
        logDir.setPath(path);
        if (!logDir.mkdir(folderName)) {
            qCritical() << "Cannot create folder" << folderName;
            abort();
        }
    }
}

//NB: Do not use logging in this method as it will generate a deadlock
void Logger::renameLogFile(bool force)
{
    QMutexLocker locker(&_mutex);
    QFileInfo fileInfo(_logFileName);
    if (fileInfo.exists()) {
        _logFile.close();//make sure that the file is closed
        if (0 != fileInfo.size()) {//don't rename empty files
            QDate date = fileInfo.lastModified().date();
            if (force || (date != QDateTime::currentDateTime().date())) {//don't rename if the log file has been accessed today
                if (date == QDateTime::currentDateTime().date()) {
                    date = date.addDays(-1);//this might happen when midnight alarm is triggered
                }
                //make sure that renaming the log file does not override an existing one
                QString oldLogFile = _logFileName+"."+date.toString("yyyy-MM-dd");
                fileInfo.setFile(oldLogFile);
                int index = 0;
                while (fileInfo.exists()) {
                    fileInfo.setFile(oldLogFile+"."+QString::number(index++));
                }
                //rename log file
                QFile::rename(_logFileName, fileInfo.absoluteFilePath());
            }
        }
    }
    //(re)open log file
    _logFile.setFileName(_logFileName);
    if (!_logFile.open(QIODevice::WriteOnly | QIODevice::Append)) {
        abort();
    }
    setupMidnightAlarm();
}

void Logger::setupMidnightAlarm()
{
    if (qApp) {
        QDateTime curDayTime = QDateTime::currentDateTime();
        QDateTime midnight(curDayTime.addDays(1).date());
        qint64 remTime = curDayTime.msecsTo(midnight);
        QTimer::singleShot(remTime, this, SLOT(renameLogFile()));
    }
}

void Logger::debugMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QMutexLocker locker(&Logger::instance()._mutex);
    QString msgType;
    switch (type) {
    case QtDebugMsg:
        msgType = QString("Debug");
        break;
    case QtInfoMsg:
        msgType = QString("Info");
        break;
    case QtWarningMsg:
        msgType = QString("Warning");
        break;
    case QtCriticalMsg:
        msgType = QString("Critical");
        break;
    case QtFatalMsg:
        msgType = QString("Fatal");
    default:
        abort();
    }
    Logger::instance()._textStream << QDateTime::currentDateTime().toString("dd-MM-yyyy hh:mm:ss.zzz") <<
                                      "\tTID: 0x" << QString::number(reinterpret_cast<qulonglong>(QThread::currentThreadId()), 16) <<
                                      "\t" << msgType << ": " << msg
                                  #ifndef QT_NO_WARNING_OUTPUT
                                   << "(" << context.file << ":" << context.line << ", " << context.function <<")"
                                  #endif
                                      ;
    endl(Logger::instance()._textStream);
}
