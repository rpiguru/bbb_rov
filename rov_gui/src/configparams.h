#ifndef CONFIG_PARAMS_H
#define CONFIG_PARAMS_H

#include <QString>
#include <QFile>
#include <QXmlStreamReader>

/*! Configuration parameter handling from XML file. The XML file is embedded with the application binary.
 */
class ConfigParams
{
public:
    ConfigParams();
    QString organizationName() const {
        QStringRef out = nodeText("organizationName");
        return (!out.isEmpty())?out.toString():"";
    }
    QString applicationName() const {
        QStringRef out = nodeText("applicationName");
        return (!out.isEmpty())?out.toString():"";
    }
    QString applicationVersion() const {
        QStringRef out = nodeText("applicationVersion");
        return (!out.isEmpty())?out.toString():"";
    }
    QString commandUrl() const {
        QStringRef out = nodeText("commandUrl");
        return (!out.isEmpty())?out.toString():"";
    }
    QString videoUrl() const {
        QStringRef out = nodeText("videoUrl");
        return (!out.isEmpty())?out.toString():"";
    }
    QString commandButtonTemplate() const {
        QStringRef out = nodeText("commandButtonTemplate");
        return (!out.isEmpty())?out.toString():"";
    }
    QString commandTriggerTemplate() const {
        QStringRef out = nodeText("commandTriggerTemplate");
        return (!out.isEmpty())?out.toString():"";
    }
    QString commandThumbStickTemplate() const {
        QStringRef out = nodeText("commandThumbStickTemplate");
        return (!out.isEmpty())?out.toString():"";
    }
private:
    const QStringRef nodeText(const char* name) const;
    Q_DISABLE_COPY(ConfigParams)
    mutable QFile _configFile;
    mutable QXmlStreamReader _reader;
};

#endif
