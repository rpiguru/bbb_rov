#ifndef POPUP_MENU_MODEL_
#define POPUP_MENU_MODEL_

#include <QAbstractListModel>
#include <QString>
#include <QVector>

class PopupMenuModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum ProjectListRoles {
        Name = Qt::UserRole + 1,
        EntryType
    };
    enum EntryTypes {
        UNKNOWN = -1,
        CONTROL_OPTIONS = 0,
        CONTROLLER_DEADBAND,
        CONTROL_RAMPING,
        LOG_OUT,
        EXIT,
        CANCEL
    };
    Q_ENUM(EntryTypes)

    PopupMenuModel(QObject *parent = NULL);
    Q_INVOKABLE void toggleSubMenu();
    Q_PROPERTY(int count READ count NOTIFY countChanged)

    int count() const {
        return _entry.count();
    }

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QHash<int,QByteArray> roleNames() const;
signals:
    void countChanged();
private:
    struct Entry {
        Entry() : type(UNKNOWN) {}
        QString name;
        int type;
    };
    QVector<Entry> _entry;
};

#endif
