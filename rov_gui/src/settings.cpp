#include <QSettings>
#include <QDebug>
#include "settings.h"
#include "configparams.h"

Settings::Settings()
{
    ConfigParams config;
    _orgName = config.organizationName();
    _appName = config.applicationName();
    _appVersion = config.applicationVersion();
    _commandUrl = config.commandUrl();
    _videoUrl = config.videoUrl();
    _commandButtonTemplate = config.commandButtonTemplate();
    _commandTriggerTemplate = config.commandTriggerTemplate();
    _commandThumbStickTemplate = config.commandThumbStickTemplate();
}

QString Settings::commandButtonTemplate(const QString &button, bool action)
{
    return _commandButtonTemplate.arg(button).arg(action?"on":"off");
}

QString Settings::commandTriggerTemplate(const QString &trigger, int value)
{
    return _commandTriggerTemplate.arg(trigger).arg(value);
}

QString Settings::commandThumbStickTemplate(const QString &thumbStrick, int xValue, int yValue)
{
    return _commandThumbStickTemplate.arg(thumbStrick).arg(xValue).arg(yValue);
}
