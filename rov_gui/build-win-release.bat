rmdir /S /Q  build
if %errorlevel% neq 0 exit /b %errorlevel%

mkdir build
cd build
if %errorlevel% neq 0 exit /b %errorlevel%

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64
if %errorlevel% neq 0 exit /b %errorlevel%

cmake .. -DCMAKE_PREFIX_PATH=C:/Qt/5.7/msvc2015_64 -G "NMake Makefiles" -DCMAKE_BUILD_TYPE=Release -DVC_REDIST_PATH="C:/Program Files (x86)/Microsoft Visual Studio 14.0/VC/redist/1033"
if %errorlevel% neq 0 exit /b %errorlevel%

nmake
if %errorlevel% neq 0 exit /b %errorlevel%

nmake package
if %errorlevel% neq 0 exit /b %errorlevel%
