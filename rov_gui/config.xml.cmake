<?xml version="1.0" encoding="utf-8"?>
<settings>
    <organizationName>Mobivs</organizationName>
    <applicationName>Remote Operating Vehicle</applicationName>
    <applicationVersion>${ROV_VERSION}</applicationVersion>
    <commandUrl>ws://216.177.177.179:5555/rov/control</commandUrl>
    <videoUrl>rtsp://ROV:roveruser@216.177.177.179/11</videoUrl>
    <commandButtonTemplate>{'componentType':'Button','Button':'%1','Action':'%2'}</commandButtonTemplate>
    <commandTriggerTemplate>{'componentType':'Trigger','Trigger':'%1','Value':%2}</commandTriggerTemplate>
    <commandThumbStickTemplate>{'componentType':'ThumbStrick','ThumbStrick':'%1','XValue':%2,'YValue':%3}</commandThumbStickTemplate>
</settings>
