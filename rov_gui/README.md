== Requirements ==

- cmake 3.0.0+
- Qt 5.7
- VLC SDK v.2.2.4 (http://download.videolan.org/pub/videolan/vlc/2.2.4/win64/vlc-2.2.4-win64.7z)
- Visual Studio Community 2015 (optional)
- NSIS (http://nsis.sourceforge.net/Main_Page) (optional)

== Compilation instructions on Windows 10 x64 ==

- unzip vlc-2.2.4-win64.7z and make sure that VLC_SDK_DIR from CMakeLists.txt
file points to the correct location
- open CMakeLists.txt file with QtCreator then build and run the application
Note that when you open the CMakeLists.txt file QtCreator allows you to select the Qt version to use. Make sure that you select Qt 5.7.
    

== Running the application ==

- make sure that the PATH environment variable contains the path where libvlc.dll resides
- after the application is started a log file is created in the user home path
in '.rov/log'. The current log file has the name 'app.log'.
- application settings can be found in 'config.xml.cmake'. After the settings
  are changed you must recompile the application.

== Installer generation ==

- use the provided script:
    build-win-release.bat
This script assumes that Visual Studio and NSIS are installed.
